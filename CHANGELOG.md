# Changelog

## Version 1.3.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
